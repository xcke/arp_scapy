from scapy.all import *


def mac2ipv6(mac="F8:59:71:4B:68:5B"):
    # only accept MACs separated by a colon
    parts = mac.split(":")
    eui64 = parts[0] + parts[1] + ":" + parts[2] + "ff" + ":" + "fe" + parts[3] + ":" + parts[4] + parts[5]
    ll_eui64 = "fe80" + "::" + eui64
    return ll_eui64

t = mac2ipv6()
print(t)