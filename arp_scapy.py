#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Copyright (c) 2018 Gabor, Kis-Hegedus
# All Rights Reserved.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY AND ALL EXPRESS OR IMPLIED
# WARRANTIES ARE DISCLAIMED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF TITLE, MERCHANTABILITY, AGAINST INFRINGEMENT, AND FITNESS
# FOR A PARTICULAR PURPOSE.
#
"""Project

A small project to generate ARP requests. This might be used to check the max ARP cap of a Cisco device :-)

"""

from scapy.all import *
import ipaddress
from netmiko import ConnectHandler
import datetime
import argparse
import logging as log
import os
import sys
import threading
import signal

""" Statics"""
TARGET_IF = 'Intel(R) Ethernet Connection (4) I219-LM'
SELF_MAC = 'D4:81:D7:F2:26:32'
DST_MAC = 'cc:46:d6:82:23:07'
BCAST_MAC = 'ff:ff:ff:ff:ff:ff'
TARGET_SUBNET = ipaddress.ip_network("15.0.0.0/8")
TARGET_IP = "15.0.0.1"
TARGET_IPV6 = "fe80::1234"
MAX_HOST = 10000000
ROUTER_USERNAME = "teszt"
ROUTER_PASS = "Teszt123"
def datetimenow():
    now = datetime.datetime.now()
    return str(now.strftime("%Y-%m-%d %H:%M"))

def create_ARP_request_probe(ipaddr_to_probe):
    arp = ARP(psrc='0.0.0.0', hwsrc=SELF_MAC, pdst=ipaddr_to_probe)
    return Ether(dst=DST_MAC)/arp

def create_ARP_request_gratuitous(ipaddr_to_broadcast):
    arp = ARP(psrc=ipaddr_to_broadcast, hwsrc=SELF_MAC, pdst=ipaddr_to_broadcast)
    return Ether(dst=BCAST_MAC)/arp

def mac2ipv6(mac="F8:59:71:4B:68:5B"):
    # only accept MACs separated by a colon
    parts = mac.split(":")
    eui64 = parts[0] + parts[1] + ":" + parts[2] + "ff" + ":" + "fe" + parts[3] + ":" + parts[4] + parts[5]
    ll_eui64 = "fe80" + "::" + eui64
    return ll_eui64

def create_ARP_ICMP_NA_request_directed(
        ipaddr_target,          # this is the target machine, set this to its actual IP address
        ipaddr_src_spoof,       # this is what the target machine will think this ARP packet comes from, might be spoofed
        eth_dest=BCAST_MAC):    # either leave as is (broadcast), or set it to the target machine's actual MAC address
    hwsrc=str(RandMAC())
    """ IPv6 ND"""
    ipv6_src = mac2ipv6(hwsrc)
    base = IPv6(src=ipv6_src,dst=TARGET_IPV6)
    ipv6_nd = ICMPv6ND_NA(tgt=ipv6_src, R=0, O=1, S=1)
    lld = ICMPv6NDOptDstLLAddr(lladdr=hwsrc)
    ipv6nd_p = base / ipv6_nd / lld
    arp_p = ARP(psrc=ipaddr_src_spoof, hwsrc=hwsrc, pdst=ipaddr_target)
    eth = Ether(src=hwsrc,dst=eth_dest)
    return eth/arp_p, eth/ipv6nd_p

def poison_target():
    """ Function to send random ARP packets"""
    host_num = MAX_HOST
    pkt_list = []
    s = conf.L2socket(iface=TARGET_IF)
    while True:
        rnd = random.randrange(10, host_num, 1)
        spoofed_ip = TARGET_SUBNET[rnd]
        spoofed_arp_p,spoofed_ipv6nd_p = create_ARP_ICMP_NA_request_directed(TARGET_IP, str(spoofed_ip), DST_MAC)
        s.send(spoofed_arp_p)
        s.send(spoofed_ipv6nd_p)
        #time.sleep(1)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-D", "--debug", help="Enable debug mode", action="store_true")
    args = parser.parse_args()
    if args.debug:
        log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)
        log.getLogger("paramiko").setLevel(log.INFO)
        log.getLogger("netmiko").setLevel(log.INFO)
        log.info("Verbose output.")
    else:
        log.basicConfig(format="%(levelname)s: %(message)s", level=log.INFO)
    net_connect = ConnectHandler(device_type='cisco_ios', ip=TARGET_IP,
                                 username=ROUTER_USERNAME,
                                 password=ROUTER_PASS,
                                 verbose='False',
                                 global_delay_factor=0)
    net_connect.enable()
    try:
        """ Start the Spoofing Thread"""
        poison_thread = threading.Thread(target=poison_target)
        poison_thread.start()
        while True:
            try:
                output = net_connect.send_command("show arp summary")
                output_ipv6 = net_connect.send_command("show ipv6 cef summary")
            except OSError:
                log.info("Target host died")
                sys.exit(0)

            log.info("[*] - {} - ".format(datetimenow()))
            log.info(output)
            log.info(output_ipv6)
            time.sleep(15)

    except KeyboardInterrupt:
        sys.exit(0)



if __name__ == '__main__':
    main()
